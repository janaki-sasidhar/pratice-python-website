def main():
    try:
        string=input("Enter the string  ")
        if string==string[::-1]:
            print(f"String  '{string}' is a palindrome ")
        else: 
            print(f"String '{string}' is not a palindrome")
    except Exception as e:
        print(e)
        main()
    
if __name__ == "__main__":
    main()