def main():
    try:
        number = int(input("Enter the number   "))
        old_list = list(range(1,number+1))
        newlist = [i for i in old_list if number%i == 0]
        print(newlist)
    except Exception as e:
        print(e)
        main()

if __name__ == "__main__":
    main()