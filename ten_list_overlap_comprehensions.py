import random
def main():
    list_one = [random.randint(1,30) for i in range(random.randint(20,40))]
    list_two = [random.randint(1,30) for i in range(random.randint(20,40))]
    result = [i for i in list_one if i in list_two]
    print(result)
if __name__ == "__main__":
    main()
