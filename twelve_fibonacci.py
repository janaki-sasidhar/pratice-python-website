def fibonacci():
    try:
        number=int(input("Enter the number of fibonacci digits you want to print ..."))
        a=0
        b=1
        count=0
        while True:
            count+=1
            if count>number:
                break
            print(a)
            a,b=b,a+b
            
    except Exception as e:
        print(e)
        fibonacci()
if __name__ == "__main__":
    fibonacci()