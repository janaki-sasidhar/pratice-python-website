def main():
    rand_list=[x*x for x in range(11)]
    newlist=[i for i in rand_list if i%2==0]
    return newlist

if __name__ == "__main__":
    print(main())