import random,sys
def main():
    request=input("Do you want to play the game ? (yes/y/no/n) ... ").lower()
    if request=='y' or request == 'yes':
        print("You have three guesses to guess the number")
        guesses_left=3
        randomly_choosed = random.randint(0,20)
        while guesses_left>0:
            try:
                inp = int(input("Enter the number  "))
                if inp == randomly_choosed:
                    guesses_left -=1
                    print(f"You guessed the correct number --{randomly_choosed}-- in --{guesses_left}-- guesses")
                    break
                else:
                    if inp < randomly_choosed:
                        print("The number is slightly bigger")
                        guesses_left-=1
                        continue
                    else:
                        print("The number is slightly lesser")
                        guesses_left -=1
                        continue
            except Exception as e:
                print(e)
        print(f"The correct answer is {randomly_choosed} ")
        again = input("Do you want to play again ? (yes/y/no/n ... ").lower()
        if again =="yes" or again=="y":
            main()
        else:
            sys.exit()
    else: 
        print("Okay , you can play some other time")
if __name__ == "__main__":
    main()