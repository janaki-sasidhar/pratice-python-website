def check(number):
    if number%2 == 0:
        print(f"The number {number} is even")
    else:
        print(f"The number {number} is odd")

def main():
    try:
        num=int(input("Enter the number "))
        check(num)
    except Exception as e:
        print(e)
        main()
if __name__ == "__main__":
    main()
